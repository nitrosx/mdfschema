/*
 *
 */

var mdfSchemaFinalize = function (key, value) {
/**
 * @function
 * @name mdfSchemaFinalize
 * @private
 * @param {string} key The key that was emitted/returned from our map/reduce functions
 * @param {object} value The object that was returned from our reduce function
 */
  return result = {
    mdf_type: key.mdf_type,
    value_type: key.value_type,
    data_type: key.data_type,
    field: key.field,
    count: value
  };
};

var mdfSchemaMap = function() {
  /**
   * @function
   * @name mdfSchemaMap
   * @private
   */
  var key, keys, dataInfo, metadataInfo, count, valueType;

  var mdfSchemaGetObjectMetadataKeys = function(node, keyString, keyInfo) {
    /**
     * @function
     * @name mdfSchemaGetObjectMetadataKeys
     * @private
     * @param {object} node The node from which we generate our keys
     * @param {string} keyString A string representing the current key 'path'
     * @param {object} keyInfo The struct that contains all our 'paths' as the key, and a count for it's value
     */
    var key, newKeyString, valueType = false;

    // Store the mongo type. 'bson' instead of 'object', etc
    valueType = ({}).toString.call(node).match(/\s([a-zA-Z]+)/)[1].toLowerCase();

    // We need to handle objects and arrays by calling getKeyInfo() recursively
    if (['array', 'bson', 'object'].indexOf(valueType) >= 0) {
  
      // Loop through each key
      for (key in node) {
        // check if we need to go deeper
        // aka if the value is a nested structure
        if (node.hasOwnProperty(key)) {
          // if the value of this key is an array,
          // provide a comulative report for all elements
          if (valueType === 'array') {
            newKeyString = keyString + '.$';
          } else {
            // everything else keep building the key with normal dot notation
            newKeyString = (keyString === '' ? key : keyString + '.' + key);
          } 
          // descend in to the object
          keyInfo = mdfSchemaGetObjectMetadataKeys(node[key], newKeyString, keyInfo);          
        }
      }
    }

    // We don't need to emit this key if it is empty
    // return output structure as it is
    if (keyString === '') {
      return keyInfo;
    }
             
    // We need to emit this key
    //
    // check if it is the first time that we encounter this key            
    if (!keyInfo.hasOwnProperty(keyString)) {
      // insert in the output structure
      keyInfo[keyString] = {};
    }
  
    // check if it is the first time that we encounter this key with this type
    if (!keyInfo[keyString].hasOwnProperty(valueType)) {
      // initialize key,type structure
      keyInfo[keyString][valueType] = 1;
    }
    return keyInfo;
  };

  // Get our keyInfo struct
  metadataInfo = mdfSchemaGetObjectMetadataKeys(this.mdf_metadata, '', {});

  // Loop through keys, emitting info
  for (metadataKey in metadataInfo) {
    // not sure why, but we check that they key is present in the array
    if (metadataInfo.hasOwnProperty(metadataKey)) {
      // loop through the key, type
      for (valueType in metadataInfo[metadataKey]) {
        // again we check if type exists in array
        if (metadataInfo[metadataKey].hasOwnProperty(valueType)) {
          // emit individual key
          key = {
            mdf_type: this.mdf_def.mdf_type,
            value_type: valueType,
            data_type: 'metadata',
            field: metadataKey
          };
          emit(key, metadataInfo[metadataKey][valueType]);
        }
      }
      // insert total information for this key
      key = {
        mdf_type: this.mdf_def.mdf_type,
        value_type: 'all',
        data_type: 'metadata',
        field: metadataKey
      };
      // emit key, value
      emit(key, 1);
    }
  }

  // works on the data properties
  for ( dataKey in this.mdf_def.mdf_data ) {
    // skip mdf keys
    if ( !dataKey.startsWith('mdf_') ) {
      // emit individual key
      key = {
        mdf_type: this.mdf_def.mdf_type,
        value_type: this.mdf_def.mdf_data[dataKey].mdf_class,
        data_type: 'data',
        field: dataKey
      };
      emit(key, 1);
      // insert total information for this key
      key = {
        mdf_type: this.mdf_def.mdf_type,
        value_type: 'all',
        data_type: 'data',
        field: dataKey
      };
      // emit key, value
      emit(key, 1);
    }
  }


  // insert total information for this key
  key = {
    mdf_type: this.mdf_def.mdf_type,
    value_type: 'all',
    data_type: 'all',
    field: 'all'
  };
  emit(key, 1);
}

var mdfSchemaReduce = function(key, values) {
  /**
   * @function
   * @name mdfSchemaReduce
   * @private
   * @param {string} key The key that was emitted from our map function
   * @param {array} values An array of values that was emitted from our map function
   */
  return Array.sum(values);
}


/*
 * run map reduce
 */
printjson(
  db.runCommand({
    mapReduce: "bladder",
    map: mdfSchemaMap,
    reduce: mdfSchemaReduce,
    finalize: mdfSchemaFinalize,
    out: { inline: 1 } 
}));

