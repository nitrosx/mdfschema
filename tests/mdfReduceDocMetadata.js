/**
 * @function
 * @name reduce
 * @private
 * @param {string} key The key that was emitted from our map function
 * @param {array} values An array of values that was emitted from our map function
 */
db.system.js.save(
  {
    _id: "mdfReduceDocMetadata",
    value : function(key, values) {
      // initialize output
      var result = {};
      // loop on all the values
      values.forEach(
        function (value) {
          var type;
          // loop on types within this key
          for (type in value) {
            // always check if key exists in array
            if (value.hasOwnProperty(type)) {
              // check if it is the first time that we encounter this type
              if (!result.hasOwnProperty(type)) {
                result[type] = { 
                  docs : 0, 
                  perDoc : 0 };
              }
              // update total number of docs and docs in which this key,type appears
              result[type].docs += value[type].docs;
              result[type].perDoc += value[type].perDoc;
            }
          }
        }
      );
      return result;
    }
  }
)


