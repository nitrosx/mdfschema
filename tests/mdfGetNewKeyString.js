/**
 * @function
 * @name getNewKeyString
 * @private
 * @param {string} key The current key
 * @param {string} keyString The object to inspect
 * @description return the key completed with key string if provided
 */
db.system.js.save(
  {
    _id: "mdfGetNewKeyString",
    value : function (key, keyString) {
      
      newKeyString = (keyString === '' ? key : keyString + '.' + key);

      return newKeyString;
    }
  }
)

