/*
 *
 */

var mdfSchemaFinalize = function (key, value) {
/**
 * @function
 * @name mdfSchemaFinalize
 * @private
 * @param {string} key The key that was emitted/returned from our map/reduce functions
 * @param {object} value The object that was returned from our reduce function
 */
  return result = {
    mdf_type: key.mdf_type,
    value_type: key.value_type,
    data_type: key.data_type,
    field: key.field,
    count: value
  };
};


var mdfGetType = function(obj) {
  /**
   * @function
   * @name mdfGetType
   * @private
   * @description returns the type of the object
   * @param {object} obj The object to inspect
   */
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};

var mdfSchemaGetObjectMetadataKeys = function(node, keyString, keyInfo) {
  /**
   * @function
   * @name mdfSchemaGetObjectMetadataKeys
   * @private
   * @param {object} node The node from which we generate our keys
   * @param {string} keyString A string representing the current key 'path'
   * @param {object} keyInfo The struct that contains all our 'paths' as the key, and a count for it's value
   */
  var key, newKeyString, valueType = false;

  // Store the mongo type. 'bson' instead of 'object', etc
  valueType = mdfGetType(node);

  // We need to handle objects and arrays by calling getKeyInfo() recursively
  if (['array', 'bson', 'object'].indexOf(valueType) >= 0) {

    // Loop through each key
    for (key in node) {
      // check if we need to go deeper
      // aka if the value is a nested structure
      if (node.hasOwnProperty(key)) {
        // if the value of this key is an array,
        // provide a comulative report for all elements
        if (valueType === 'array') {
          newKeyString = keyString + '.$';
        } else {
          // everything else keep building the key with normal dot notation
          newKeyString = mdfGetNewKeyString(key, keyString);
        } 
        // descend in to the object
        keyInfo = mdfSchemaGetObjectMetadataKeys(node[key], newKeyString, keyInfo);          
      }
    }
  }

  // We don't need to emit this key if it is empty
  // return output structure as it is
  if (keyString === '') {
    return keyInfo;
  }
           
  // We need to emit this key
  //
  // check if it is the first time that we encounter this key            
  if (!keyInfo.hasOwnProperty(keyString)) {
    // insert in the output structure
    keyInfo[keyString] = {};
  }

  // check if it is the first time that we encounter this key with this type
  if (!keyInfo[keyString].hasOwnProperty(valueType)) {
    // initialize key,type structure
    keyInfo[keyString][valueType] = 1;
  }
  return keyInfo;
}

var mdfSchemaMap = function() {
  /**
   * @function
   * @name mdfSchemaMap
   * @private
   */
  var key, keys, dataInfo, metadataInfo, count, valueType;

  // Get our keyInfo struct
  metadataInfo = mdfSchemaGetObjectMetadataKeys(this.mdf_metadata, '', {});

  // Loop through keys, emitting info
  for (metadataKey in metadataInfo) {
    // not sure why, but we check that they key is present in the array
    if (metadataInfo.hasOwnProperty(metadataKey)) {
      // initialize total occurences counter
      count = 0;
      // loop through the key, type
      for (valueType in metadataInfo[metadataKey]) {
        // again we check if type exists in array
        if (metadataInfo[metadataKey].hasOwnProperty(valueType)) {
          // emit individual key
          key = {
            mdf_type: this.mdf_def.mdf_type,
            value_type: valueType,
            data_type: 'metadata',
            field: metadataKey
          };
          emit(key, metadataInfo[metadataKey][valueType]);
        }
      }
      // insert total information for this key
      key = {
        mdf_type: this.mdf_def.mdf_type,
        value_type: 'all',
        data_type: 'metadata',
        field: metadataKey
      };
      // emit key, value
      emit(key, count);
    }
  }
  // insert total information for this key
  key = {
    mdf_type: this.mdf_def.mdf_type,
    value_type: 'all',
    data_type: 'all',
    field: 'all'
  };
  emit(key, 1);
}

var mdfSchemaReduce = function(key, values) {
  /**
   * @function
   * @name mdfSchemaReduce
   * @private
   * @param {string} key The key that was emitted from our map function
   * @param {array} values An array of values that was emitted from our map function
   */
  /*
  // initialize output
  var result = 0;
  // loop on all the values
  values.forEach(
    function (value) {
      result += value;
    }
  );
  return result;
  */
  return Array.sum(values);
}


/*
 * run map reduce
 */
db.runCommand({
  mapReduce: "mdfDbTest",
  map: mdfSchemaMap,
  reduce: mdfSchemaReduce,
  finalize: mdfSchemaFinalize,
  out: { inline: 1 } 
});

