/**
 * @function
 * @name mdfSchemaGetObjectMetadataKeys
 * @private
 * @param {object} node The node from which we generate our keys
 * @param {string} keyString A string representing the current key 'path'
 * @param {object} keyInfo The struct that contains all our 'paths' as the key, and a count for it's value
 */
db.system.js.save(
  {
    _id: "mdfSchemaGetObjectMetadataKeys",
    value : function(node, keyString, keyInfo) {
      var key, newKeyString, valueType = false;

      // Store the mongo type. 'bson' instead of 'object', etc
      valueType = mdfGetType(node);

      // We need to handle objects and arrays by calling getKeyInfo() recursively
      if (['array', 'bson', 'object'].indexOf(valueType) >= 0) {

        // Loop through each key
        for (key in node) {
          // check if we need to go deeper
          // aka if the value is a nested structure
          if (node.hasOwnProperty(key)) {
            // if the value of this key is an array,
            // provide a comulative report for all elements
            if (valueType === 'array') {
              newKeyString = keyString + '.$';
            } else {
              // everything else keep building the key with normal dot notation
              newKeyString = mdfGetNewKeyString(key, keyString);
            }
            // descend in to the object
            keyInfo = mdfGetDocMetadataKeys(node[key], newKeyString, keyInfo);
          }
        }
      }

      // We don't need to emit this key if it is empty
      // return output structure as it is
      if (keyString === '') {
        return keyInfo;
      }
            
      // We need to emit this key
      //
      // check if it is the first time that we encounter this key            
      if (!keyInfo.hasOwnProperty(keyString)) {
        // insert in the output structure
        keyInfo[keyString] = {};
      }

      // check if it is the first time that we encounter this key with this type
      if (!keyInfo[keyString].hasOwnProperty(valueType)) {
        // initialize key,type structure
        keyInfo[keyString][valueType] = {
          docs : 1
        };
      }

      // update key,type counter
      keyInfo[keyString][valueType].perDoc += 1;

      return keyInfo;
    }
  }
)
