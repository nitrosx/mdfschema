/*
 *
 */
res = db.mdfDbTest.mapReduce(
  mdfMapDocMetadata,
  mdfReduceDocMetadata,
  {
   out: { inline: 1},
   finalize: mdfFinalizeDocMetadata,
   scope: {
     totalNumberOfDocuments: db.mdfDbTest.stats().count,
   },
  }
)

