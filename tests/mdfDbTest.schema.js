{
	"results" : [
		{
			"_id" : {
				"mdf_type" : "dummy",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all"
			},
			"value" : {
				"mdf_type" : "dummy",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all",
				"count" : 2
			}
		},
		{
			"_id" : {
				"mdf_type" : "dummy",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "name"
			},
			"value" : {
				"mdf_type" : "dummy",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "name",
				"count" : 2
			}
		},
		{
			"_id" : {
				"mdf_type" : "dummy",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "name"
			},
			"value" : {
				"mdf_type" : "dummy",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "name",
				"count" : 2
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "channel"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "channel",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "manufacturer"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "manufacturer",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "type"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "type",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "channel"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "channel",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "manufacturer"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "manufacturer",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "type"
			},
			"value" : {
				"mdf_type" : "electrode",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "type",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "ChanConfigMessageID"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "ChanConfigMessageID",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "DAQmod"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "DAQmod",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "block"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "block",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "channel"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "channel",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "date"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "date",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentDescription"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentDescription",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentType"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentType",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "inter"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "inter",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "location"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "location",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "maxV"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "maxV",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "minV"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "minV",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "origin"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "origin",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "rep"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "rep",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "selected"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "selected",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "session"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "session",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "set"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "set",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "status"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "status",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "stimAmp"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "stimAmp",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "subject"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "subject",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "time"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "time",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "timestamp_NSP1"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "timestamp_NSP1",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "timestamp_NSP2"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "timestamp_NSP2",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "trial"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "trial",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "boolean",
				"data_type" : "metadata",
				"field" : "selected"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "boolean",
				"data_type" : "metadata",
				"field" : "selected",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "ChanConfigMessageID"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "ChanConfigMessageID",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "channel"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "channel",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "inter"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "inter",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "maxV"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "maxV",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "minV"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "minV",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "stimAmp"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "stimAmp",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "timestamp_NSP1"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "timestamp_NSP1",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "timestamp_NSP2"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "number",
				"data_type" : "metadata",
				"field" : "timestamp_NSP2",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "DAQmod"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "DAQmod",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "block"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "block",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "date"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "date",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentDescription"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentDescription",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentType"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentType",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "location"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "location",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "origin"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "origin",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "rep"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "rep",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "session"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "session",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "set"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "set",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "status"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "status",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "subject"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "subject",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "time"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "time",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "trial"
			},
			"value" : {
				"mdf_type" : "pulse",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "trial",
				"count" : 3
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "manufacturer"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "manufacturer",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "serialNumber"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "serialNumber",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "type"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "type",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "manufacturer"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "manufacturer",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "serialNumber"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "serialNumber",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "type"
			},
			"value" : {
				"mdf_type" : "system",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "type",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "all",
				"field" : "all",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "block"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "block",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "date"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "date",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentDescription"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentDescription",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentType"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "experimentType",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "location"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "location",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "origin"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "origin",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "session"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "session",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "set"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "set",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "status"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "status",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "subject"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "subject",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "time"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "time",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "trial"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "all",
				"data_type" : "metadata",
				"field" : "trial",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "block"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "block",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "date"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "date",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentDescription"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentDescription",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentType"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "experimentType",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "location"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "location",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "origin"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "origin",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "session"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "session",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "set"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "set",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "status"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "status",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "subject"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "subject",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "time"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "time",
				"count" : 1
			}
		},
		{
			"_id" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "trial"
			},
			"value" : {
				"mdf_type" : "trial",
				"value_type" : "string",
				"data_type" : "metadata",
				"field" : "trial",
				"count" : 1
			}
		}
	],
	"timeMillis" : 15,
	"counts" : {
		"input" : 8,
		"emit" : 182,
		"reduce" : 48,
		"output" : 89
	},
	"ok" : 1
}
