/**
 * @function
 * @name mdfSchemaMap
 * @private
 */
db.system.js.save(
  {
    _id: "mdfSchemaMap",
    value : function() {
      var key, keys, dataInfo, metadataInfo, count, type;

      // Get our keyInfo struct
      metadataInfo = mdfSchemaGetObjectMetadataKeys(this.mdf_metadata, '', {});

      // Loop through keys, emitting info
      for (metadataKey in metadataInfo) {
        // not sure why, but we check that they key is present in the array
        if (metadataInfo.hasOwnProperty(metadataKey)) {
          // initialize total occurences counter
          count = 0;
          // loop through the key, type
          for (valueType in metadataInfo[metadataKey]) {
            // again we check if type exists in array
            if (metadataInfo[metadataKey].hasOwnProperty(valueType)) {
              // adds how many documents have this key,type
              count += metadataInfo[metadataKey][valueType].docs;
              // emit individual key
              key = {
               mdf_type: this.mdf_def.mdf_type,
               value_type: valueType,
               data_type: 'metadata',
               field: metadataKey
              };
              emit(key, metadataInfo[metadataKey][valueType].docs);
            }
          }
          // insert total information for this key
          key = {
           mdf_type: this.mdf_def.mdf_type,
           value_type: 'all',
           data_type: 'metadata',
           field: metadataKey
          };
          // emit key, value
          emit(key, count);
        }
      }
      // insert total information for this key
      key = {
       mdf_type: this.mdf_def.mdf_type,
       value_type: 'all',
       data_type: 'all',
       field: 'all'
      };
      emit(key, 1);
    }
  }
)

