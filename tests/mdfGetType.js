/**
 * @function
 * @name mdfGetType
 * @private
 * @description returns the type of the object
 * @param {object} obj The object to inspect
 */
db.system.js.save(
  {
    _id: "mdfGetType",
    value : function(obj) {
      return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }
  }
)

