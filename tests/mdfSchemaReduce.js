/**
 * @function
 * @name mdfSchemaReduce
 * @private
 * @param {string} key The key that was emitted from our map function
 * @param {array} values An array of values that was emitted from our map function
 */
db.system.js.save(
  {
    _id: "mdfSchemaReduce",
    value : function(key, values) {
      // initialize output
      var result = 0;
      // loop on all the values
      values.forEach(
        function (value) {
          result += value;
        }
      );
      return result;
    }
  }
)


