/**
 * @function
 * @name mdfMapDocMetadata
 * @private
 */
db.system.js.save(
  {
    _id: "mdfMapDocMetadata",
    value : function() {
      var key, keyInfo, count, type;

      // Get our keyInfo struct
      keyInfo = mdfGetDocMetadataKeys(this.mdf_metadata, this.mdf_def.mdf_type, {});

      // Loop through keys, emitting info
      for (key in keyInfo) {
        // not sure why, but we check that they key is present in the array
        if (keyInfo.hasOwnProperty(key)) {
          // initialize total occurences counter
          count = 0;
          // loop through the key, type
          for (type in keyInfo[key]) {
            // again we check if type exists in array
            if (keyInfo[key].hasOwnProperty(type)) {
              // adds how many documents have this key,type
              count += keyInfo[key][type].perDoc;
            }
          }
          // insert total information
          keyInfo[key].all = {
            docs : 1,
            perDoc : count
          };
          // emit key, value
          emit(key, keyInfo[key]);
        }
      }
    }
  }
)

