/**
 * @function
 * @name mdfSchemaFinalize
 * @private
 * @param {string} key The key that was emitted/returned from our map/reduce functions
 * @param {object} value The object that was returned from our reduce function
 */
db.system.js.save(
  {
    _id : "mdfSchemaFinalize",
    value : function (key, value) {
      var result = {
        mdf_type: key.mdf_type,
        value_type: key.value_type,
        data_type: key.data_type,
        field: key.field,
        count: value
      };

      return result;
    }
  }
)

