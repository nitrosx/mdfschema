/**
 * @function
 * @name finalize
 * @private
 * @param {string} key The key that was emitted/returned from our map/reduce functions
 * @param {object} value The object that was returned from our reduce function
 */
db.system.js.save(
  {
    _id : "mdfFinalizeDocMetadata",
    value : function (key, value) {
      var type, result = {
        mdf_type :
        types : [],
        results : [{
          type : 'all',
          docs : value.all.docs,
          coverage : (value.all.docs / totalNumberOfDocuments) * 100,
          perDoc : value.all.perDoc / value.all.docs
        }]
      };

      for (type in value) {
        if (value.hasOwnProperty(type) && type !== 'all') {
          result.types.push(type);
          result.results.push({
            type : type,
            docs : value[type].docs,
            coverage : (value[type].docs / totalNumberOfDocuments) * 100,
            perDoc : value[type].perDoc / value[type].docs
          });
        }
      }

      return result;
    }
  }
)

