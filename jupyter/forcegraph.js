// get reference to svg, width and height values
var svg = window.d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

    svg.selectAll("g").remove();

var link = svg.append("g")
    .attr("class", "links")
    .selectAll("line")
    .data(window.mdfData.links)
    .enter().append("line")
    .attr("stroke-width", function(d) { return Math.sqrt(d.value); })
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", 0)
    .attr("y2", 0);


var node = svg.append("g")
    .attr("class", "nodes")
    .selectAll("circle")
    .data(window.mdfData.nodes)
    .enter().append("circle")
    .attr("r", 5)
    .attr("cx", 0)
    .attr("cy", 0)
    .attr("fill", function(d) { return "#000"; });

node.append("title")
    .text(function(d) { return d.description; });

var simulation = window.d3.forceSimulation(window.mdfData.nodes)
    .force("link", window.d3.forceLink(window.mdfData.links)
        .id(function(d) { return d.id; }))
    .force("charge", window.d3.forceManyBody())
    .force("center", window.d3.forceCenter(width / 2, height / 2))
    .on("tick", ticked);
    

function ticked() {
    console.log(link)
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });
    console.log(link)
    node
        .attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });
}

