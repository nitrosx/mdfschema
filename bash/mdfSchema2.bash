#!/bin/bash
#
# run schema extraction from mdf data collection
# by: Max Novelli
#     man8@pitt.edu
#     2018/08/27
#
#
#

# define help function
help () {
  cat << HelpMessage

mdfSchema.bash <options>

retrieve and print on standard output the schema of the mdf data collection selected.
The schema is in jason format

Options:
  -h,--host <xxx.yyy.ttt.nnn>
    host where the mdf data collection is located
  -p,--port <nnnnn>
    port that the mdf data collection is responding to
  -d,--db,--database <database-name>
    name of the database containing the mdf data collection
  -c,--coll,--collection <collection-name>
    name f the collection dontaing the mdf data collection
  --help
    print this help and exit
  -s,--silent
    do not print the mdf data collection settings, just the schema info 

by: Max Novelli (man8@pitt.edu)
    2018/08/27   

HelpMessage
}

# get command line arguments
options=$(getopt \
  --options "h:p:d:c:s" \
  --longoptions "host:,port:,db:,database:,coll:,collection:,help,silent" \
  --name "$(basename $0)" \
  -- "$@")
# check if getopt returned an error
[ $? -eq 0 ] || {
  echo "Incorrect options provided"
  exit 1
}
# not sure why we do this
eval set -- "$options"

# set argument variables value
HOST=""
PORT=""
DB=""
COLL=""
SETTINGS=true

# interpret input arguments
while true; do
  case "$1" in
    -h | --host )
      HOST=$2
      shift 2
      ;;
    -p | --port )
      PORT=$2
      shift 2
      ;;
    -d | --db | --database )
      DB=$2
      shift 2
      ;;
    -c | --coll | --collection )
      COLL=$2
      shift 2
      ;;
    --help)
      help
      exit 1
      ;;
    -s | --silent)
      SETTINGS=false
      shift 1
      ;;
    -- ) shift; break ;;
    * )
      if [ -z "$1"]; then
        break
      else
        echo "$1 is not a valid option"
        exit 1
      fi
  esac
done

# report settings that are going to be used to collect the schema
if [ "$SETTINGS" = true ]; then
  echo ""
  echo "Connection settings:"
  echo "Host       : ${HOST}"
  echo "Port       : ${PORT}"
  echo "Database   : ${DB}"
  echo "Collection : ${COLL}" 
  echo ""
fi

if [ -z "$HOST" ] | [ -z "$PORT" ] | [ -z "$DB" ] | [ -z "$COLL" ]; then
  echo "Please provide all foour required parameters to connect to the mdf data collection"
  help
  exit 1
fi

# calls mongo shell and pass in the map/reduce code to extract mdf schema
mongo \
  --host ${HOST} \
  --port ${PORT} \
  << MongoCommands
use ${DB};
printjson(
  res = db.runCommand({
    mapReduce: "${COLL}",
    map: function(){var a,t,e,y=function(a,t,e){var d,f,m;if(m={}.toString.call(a).match(/\s([a-zA-Z]+)/)[1].toLowerCase(),0<=["array","bson","object"].indexOf(m))for(d in a)a.hasOwnProperty(d)&&(f="array"===m?t+".$":""===t?d:t+"."+d,e=y(a[d],f,e));return""===t||(e.hasOwnProperty(t)||(e[t]={}),e[t].hasOwnProperty(m)||(e[t][m]=1)),e};for(k in t=y(this.mdf_metadata,"",{}))if(t.hasOwnProperty(k)){for(e in t[k])t[k].hasOwnProperty(e)&&(a={mdf_type:this.mdf_def.mdf_type,value_type:e,data_type:"metadata",field:k},emit(a,t[k][e]));a={mdf_type:this.mdf_def.mdf_type,value_type:"all",data_type:"metadata",field:k},emit(a,1)}for(z in this.mdf_def.mdf_data)z.startsWith("mdf_")||(a={mdf_type:this.mdf_def.mdf_type,value_type:this.mdf_def.mdf_data[z].mdf_class,data_type:"data",field:z},emit(a,1),a={mdf_type:this.mdf_def.mdf_type,value_type:"all",data_type:"data",field:z},emit(a,1));a={mdf_type:this.mdf_def.mdf_type,value_type:"all",data_type:"all",field:"all"},emit(a,1)},
    reduce: function(r,n){return Array.sum(n)},
    finalize: function(t,e){return result={mdf_type:t.mdf_type,value_type:t.value_type,data_type:t.data_type,field:t.field,count:e}},
    out: { inline: 1 } 
}));
MongoCommands




  

